import React from 'react';
import { connect } from "react-redux";

import FSTable from "./FSTable";
import FSInfo from "./FSInfo";
import FSForms from "./FSForms";

import './FilmStorage.scss'

const apiURL = 'http://localhost:3001/'

class FilmStorage extends React.Component {
    state = {
        fileText: null,
        message: null,
        tableData: null,
        formType: null,
        info: null,
        find_by: 'title',
        search: ''
    }

    reloadTable = () => {

        if (this.state.search) {
            fetch(apiURL + 'films?sort=' + this.props.sorting[0] + '&dir=' + this.props.sorting[1] + '&find_by=' + this.state.find_by + '&search=' + this.state.search)
                .then(res => res.json())
                .then(data => this.setState({ tableData: data }))

            // data.filter(x => x['Title'].match(/abc/gi) || x['Stars'].match(/abc/gi)); // client search
        } else {
            fetch(apiURL + 'films?sort=' + this.props.sorting[0] + '&dir=' + this.props.sorting[1])
                .then(res => res.json())
                .then(data => this.setState({ tableData: data }))
        }
    }

    showInfo = (id) => {
        if (this.state.info && this.state.info.id === id) {
            this.setState({ info: null })
        } else {
            fetch(apiURL + 'film/' + id)
                .then(res => res.json())
                .then(data => this.setState({ info: data, formType: null }))
        }
    }

    closeContainer = (name) => {
        this.setState({ [name]: null })
    }

    fileRead = (file) => {
        if (file) {
            if (file.type === 'text/plain') {
                let fileRdr = new FileReader();
                fileRdr.onload = (loadEv) => {
                    let loadText = loadEv.target.result;
                    this.setState({ fileText: loadText, message: null });
                }
                fileRdr.readAsText(file, 'UTF-8');
            }
            else {
                this.setState({ fileText: null, message: 'Incorect file type!' });
            }
        } else {
            this.setState({ fileText: null, message: null });
        }
    }

    addSubmit = (data) => {
        fetch(apiURL + 'film', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then(res => res.json())
            .then(data => data.success && this.reloadTable(), this.setState({ formType: null }))
    }

    importSubmit = (e) => {
        e.preventDefault();
        var data = this.state.fileText;

        if (data) {
            data = data
                .split('\n\n')
                .filter(x => x.length > 0)
                .map(x => x.split('\n'))
                .map(x => x.reduce((a, x) => {
                    let b = x.indexOf(':');
                    a[x.slice(0, b).toLowerCase().replace(' ', '_')] = x.slice(b + 2);
                    return a;
                }, {}));
            // send data
            fetch(apiURL + 'films', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            })
                .then(res => res.json())
                .then(data => data.success && this.reloadTable(), this.setState({ formType: null }))
        }
    }

    delSubmit = (id) => {
        fetch(apiURL + 'film/' + id, {
            method: 'DELETE'
        })
            .then(res => res.json())
            .then(data => data.success && this.reloadTable(), this.setState({ info: null }))
    }

    formCreate = () => {
        if (this.state.formType === 'create') {
            this.setState({ formType: null })
        } else {
            this.setState({ formType: 'create', info: null })
        }
        this.setState({ message: null })
    }

    formImport = () => {
        if (this.state.formType === 'import') {
            this.setState({ formType: null })
        } else {
            this.setState({ formType: 'import', info: null })
        }
        this.setState({ message: null })
    }

    finderChange = (e) => {
        this.setState({ find_by: e.currentTarget.value })
    }
    finderInput = (e) => {
        this.setState({ search: e.currentTarget.value })
    }

    // lifecycles
    componentDidUpdate(prevProps, prevState) {
        if (prevProps.sorting !== this.props.sorting) {
            this.reloadTable();
        }
        if (prevState.search !== this.state.search) {
            this.reloadTable();
        }
    }

    componentDidMount() {
        this.reloadTable();
    }

    render() {
        return <div className='fs-wrapper'>
            <div className='fs-row-header'>
                <div className='fs-buttons-panel'>
                    <button onClick={this.reloadTable}>Update</button>
                    <button onClick={this.formCreate}>Add</button>
                    <button onClick={this.formImport}>Import</button>
                </div>
                <div className='fs-finder'>
                    <label> Search by </label>
                    <select
                        value={this.state.find_by}
                        onChange={this.finderChange}
                    >
                        <option value='title'>Title</option>
                        <option value='release_year'>Relese Year</option>
                        <option value='stars'>Star</option>
                    </select>
                    <input
                        value={this.state.search}
                        onChange={this.finderInput}
                    ></input>
                </div>
            </div>

            {
                this.state.tableData && this.state.tableData.length ?
                    <FSTable
                        data={this.state.tableData}
                        showInfo={this.showInfo}
                    /> :
                    <div>No data...</div>
            }

            {this.state.info && <FSInfo
                info={this.state.info}
                delSubmit={this.delSubmit}
                closeContainer={this.closeContainer}
            />}

            {this.state.formType && <FSForms
                formType={this.state.formType}
                apiUrl={apiURL}
                message={this.state.message}
                fn={{
                    reloadTable: this.reloadTable,
                    fileRead: this.fileRead,
                    addSubmit: this.addSubmit,
                    importSubmit: this.importSubmit,
                    closeContainer: this.closeContainer
                }} />}

        </div>
    }
}

const mapStateToProps = state => {
    return {
        sorting: state.sorting
    }
}

export default connect(mapStateToProps)(FilmStorage);