const CHANGE_SORTING = 'CHANGE_SORTING';

function updateSorting(name) {
    return {
        type: CHANGE_SORTING,
        data: { name }
    }
}

export { CHANGE_SORTING, updateSorting };