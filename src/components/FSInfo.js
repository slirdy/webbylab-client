import React from 'react';

function FSInfo({ info, delSubmit, closeContainer }) {
    let closeInfo = () => {
        closeContainer('info');
    }

    let onDeleteDown = (e) => {
        e.preventDefault();
        delSubmit(info.id)
    }

    return <div className='fs-row-info'>
        <button className='fs-close-btn' onClick={closeInfo}>×</button>
        <div>
            <div>
                <label>Title: </label>
                <span>{info.title}</span>
            </div>
            <div>
                <label>Release Year: </label>
                <span>{info.release_year}</span>
            </div>
            <div>
                <label>Format: </label>
                <span>{info.format}</span>
            </div>
            <div>
                <label>Stars: </label>
                <span>{info.stars}</span>
            </div>
            <button onClick={onDeleteDown} >Delete</button>
        </div>
    </div>
}

export default FSInfo;