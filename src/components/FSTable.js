import React from 'react';

import FSTableRow from "./FSTableRow";

function FSTable({ data, showInfo }) {
    return <div className="fs-row-table">
        <table>
            <thead>
                {<FSTableRow data={data[0]} header={true} />}
            </thead>
            <tbody>
                {data.map((row, row_idx) => <FSTableRow
                    data={row}
                    showInfo={showInfo}
                    key={row_idx} />)}
            </tbody>
        </table>
    </div>
}

export default FSTable;