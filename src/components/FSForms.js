import React from 'react';

var StarInput = ({ name, idx, addStar, removeStar, changeStar }) => {
    let addButtonClick = () => {
        addStar(idx);
    }
    let removeButtonClick = () => {
        removeStar(idx);
    }
    let inputChange = (e) => {
        let val = e.currentTarget.value;
        changeStar(val, idx)
    }

    return <div>
        <button onClick={removeButtonClick} type='button'>-</button>
        <input
            onChange={inputChange}
            value={name}
            name='star'
            className='dynamical-input'
            required
        ></input>
        <button onClick={addButtonClick} type='button'>+</button>
    </div>
}

class FSForms extends React.Component {
    state = {
        formData: {},
        starsNames: ['']
    }

    getFileMsgRef = React.createRef();

    removeStar = (idx) => {
        if (this.state.starsNames.length < 2) return;
        let arr = [...this.state.starsNames];
        arr.splice(idx, 1);
        this.setState({ starsNames: arr });
    }
    addStar = (idx) => {
        let arr = [...this.state.starsNames];
        arr.splice(idx + 1, 0, '');
        this.setState({ starsNames: arr });
    }
    changeStar = (val, idx) => {
        let arr = [...this.state.starsNames];
        arr[idx] = val;
        this.setState({ starsNames: arr })
    }
    fileChoose = (e) => {
        this.getFileMsgRef.current.value = e.currentTarget.value;
        this.props.fn.fileRead(e.currentTarget.files[0]);
    }
    saveToData = (e) => {
        let target = e.currentTarget;
        let obj = { ...this.state.formData };
        obj[target.name] = target.value;
        this.setState({ formData: obj });
    }

    completeFormSubmit = (e) => {
        e.preventDefault();
        let data = this.state.formData;
        data['stars'] = this.state.starsNames.join(', ');
        this.props.fn.addSubmit(data);
    }

    closeForm = () => {
        this.props.fn.closeContainer('formType')
    }

    render() {
        return <div className='fs-row-add'>
            <button className='fs-close-btn' onClick={this.closeForm}>×</button>

            {/* creation|add form */}
            {
                this.props.formType === 'create' && <div className='fs-create'>
                    <form onSubmit={this.completeFormSubmit}>
                        <div>
                            <label>Title</label>
                            <input name="title" onChange={this.saveToData} required></input>
                        </div>
                        <div>
                            <label>Release Year</label>
                            <input
                                onChange={this.saveToData}
                                name="release_year"
                                type='number'
                                min='1895'
                                max={new Date().getFullYear()}
                                required
                            ></input>
                        </div>
                        <div>
                            <label>Format</label>
                            <select name="format" onChange={this.saveToData} required>
                                <option hidden value=''>Select Format</option>
                                <option value='VHS'>VHS</option>
                                <option value='DVD'>DVD</option>
                                <option value='Blu-Ray'>Blu-Ray</option>
                            </select>
                        </div>
                        <div>
                            <label>Stars</label>
                            <div className='dynamical-input-block'>{this.state.starsNames.map((name, i) => <StarInput
                                name={name}
                                idx={i}
                                addStar={this.addStar}
                                removeStar={this.removeStar}
                                changeStar={this.changeStar}
                                //
                                key={i}
                            />
                            )}</div>
                        </div>
                        <div>
                            <button type='submit'>Confirm</button>
                        </div>
                    </form>
                </div>
            }

            {/* import form */}
            {
                this.props.formType === 'import' &&
                <div className='fs-import'>
                    <form onSubmit={this.props.fn.importSubmit}>
                        <label className='file-input-button'>
                            Select file
                            <input type='file' accept='text/plain' onChange={this.fileChoose} />
                        </label>
                        <div className='file-input-message'>
                            <input ref={this.getFileMsgRef} placeholder='No file selected' disabled />
                        </div>
                        {this.props.message && <div className='fs-message'>{this.props.message}</div>}
                        <button type='submit'>Import</button>
                    </form>
                </div>
            }

            {/*TODO: edit form ?? */}
        </div >
    }
}

export default FSForms;