import React from 'react';

import FSTableCell from "./FSTableCell";

function FSTableRow({ data, header, showInfo }) {
    function onRowClick() {
        showInfo(data.id);
    }

    return <tr onClick={header ? undefined : onRowClick}>
        {Object.keys(data).map((key, cell_idx) => <FSTableCell
            data={[key, data[key]]}
            header={header}
            key={cell_idx}
        />)}
    </tr>
}

export default FSTableRow;