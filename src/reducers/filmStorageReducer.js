import { CHANGE_SORTING } from "../actions/filmStorageAction";

let sorting = ['id', 'ASC']; // default sorting

const initialState = {
    sorting
}

function filmStorageReducer(state = initialState, action) {
    switch (action.type) {
        case CHANGE_SORTING:
            let { name } = action.data;
            let sorting = [...state.sorting];

            if (name === sorting[0]) {
                if (sorting[1] === 'ASC') {
                    sorting[1] = 'DESC';
                } else {
                    sorting[1] = 'ASC';
                }
            } else {
                sorting = [name, 'ASC']
            }
            return { ...state, sorting };
        default:
            return state;
    }
}

export default filmStorageReducer;