import React, { Component } from 'react';

import FilmStorage from "./components/FilmStorage";
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <FilmStorage />
      </div>
    );
  }
}

export default App;
