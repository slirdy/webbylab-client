import React from 'react';
import { connect } from 'react-redux';

import { updateSorting } from "../actions/filmStorageAction";

class FSTableCell extends React.Component {

    cellClick = () => {
        this.props.updateSortingX(this.props.data[0])
    }

    render() {
        return <td onClick={this.props.header && this.cellClick}>
            {this.props.header ? this.props.data[0].toUpperCase() : this.props.data[1]}
        </td>
    }
}

const mapDispathToProps = dispatch => {
    return {
        updateSortingX: (name) => dispatch(updateSorting(name))
    }
}

export default connect(null, mapDispathToProps)(FSTableCell);